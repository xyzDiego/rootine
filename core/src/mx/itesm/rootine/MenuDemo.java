package mx.itesm.rootine;

/**
 * Created by prez on 2/25/18.
 */

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;

public class MenuDemo extends Game {
    private final AssetManager assetManager = new AssetManager();

    public MenuDemo() {
    }

    public void create() {
        this.setScreen(new IntroScreen(this));
    }
    public AssetManager getAssetManager() {
        return assetManager;
    }

}