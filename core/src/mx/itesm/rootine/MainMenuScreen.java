package mx.itesm.rootine;

/*
Shit to do:
Add hiscore in top corner
Add Sfx for buttons
Code 1 minigame
 */

		import com.badlogic.gdx.Game;
		import com.badlogic.gdx.Gdx;
        import com.badlogic.gdx.Preferences;
        import com.badlogic.gdx.ScreenAdapter;
        import com.badlogic.gdx.audio.Music;
        import com.badlogic.gdx.audio.Sound;
        import com.badlogic.gdx.graphics.Color;
		import com.badlogic.gdx.graphics.Texture;
        import com.badlogic.gdx.graphics.g2d.BitmapFont;
        import com.badlogic.gdx.graphics.g2d.GlyphLayout;
        import com.badlogic.gdx.graphics.g2d.SpriteBatch;
        import com.badlogic.gdx.graphics.g2d.TextureRegion;
        import com.badlogic.gdx.graphics.glutils.ShaderProgram;
        import com.badlogic.gdx.math.Interpolation;
        import com.badlogic.gdx.math.MathUtils;
        import com.badlogic.gdx.math.Rectangle;
        import com.badlogic.gdx.scenes.scene2d.InputEvent;
		import com.badlogic.gdx.scenes.scene2d.Stage;
		import com.badlogic.gdx.scenes.scene2d.actions.Actions;
        import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
        import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
		import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
        import com.badlogic.gdx.utils.Align;
        import com.badlogic.gdx.utils.viewport.FitViewport;


public class MainMenuScreen extends ScreenAdapter {


    //Define screen size, stage

	private final Game game;
    private float xx = Gdx.graphics.getWidth();
    private float yy = Gdx.graphics.getHeight();
	private Stage stage;



    //Textures for backgrounds
	private Texture backgroundTexture;
    private Texture backgroundSupplies1Texture;
    private Texture backgroundSupplies2Texture;
    private Texture backgroundCharacterTexture;

    //Background Controllers
    private Rectangle bgSupplies1Controller;
    private Rectangle bgSupplies2Controller;
    private Rectangle bgCharacterController;
    private String bgState;
    private Double bgSin;
    private Double startTransp;

    //Menu Textures
	private Texture titleTexture;
    private Float titleAlpha;

	private Texture settingsBtnTexture;
    private Texture settingsBtnPressedTexture;


	//Transition Texture, Variables
	private Texture transitionTexture;
	private Texture transitionBackground;
	private String transition;
	private Rectangle transitionController;
	private String transitionToNext;
    private Preferences prefs;

    private SpriteBatch batch;
    private ImageButton settingsBtn;

    private Float transitionAlpha;
    private Integer roomvisits;

    BitmapFont bmpFnt;
    BitmapFont bmpFnt2;

    GlyphLayout layout;

    private String vs;
    private String fs;
    private ShaderProgram shaderProgram;
    private Integer score;
    private Sound bgmsc;
    private Boolean bgmStarted;

    public MainMenuScreen(Game game) {
		this.game = game;
	}

	public void show() {
		super.show();
		Gdx.app.log(String.valueOf(xx),String.valueOf(yy));
		//Room visits - DEBUG
        prefs = Gdx.app.getPreferences("MyPreferences");
        roomvisits = prefs.getInteger("roomvisits",0);
        roomvisits +=1;
        Gdx.app.log("Room visits",String.valueOf(roomvisits));
        prefs.putInteger("roomvisits",roomvisits);
        //Reset Score
        prefs.putInteger("score",0);

        prefs.putFloat("gameSpeed",1.f);
        prefs.putString("LastGame","win");

        score = prefs.getInteger("hiscore",0);
        //Reset Minigame number
        prefs.putInteger("minigamesNo",0);
        //Reset Day number
        prefs.putInteger("dayNo",1);
        //Reset Lives
        prefs.putInteger("livesNo",3);
        prefs.putInteger("prevlivesNo",3);
        bgmStarted = prefs.getBoolean("bgmStarted",true);
        prefs.flush();

        this.bgmsc = Gdx.audio.newSound(Gdx.files.internal("sMusic.wav"));
        bgmsc.play(1.f);



        //Set Stage
		this.stage = new Stage(new FitViewport(1280.0F, 720.0F));

		Gdx.input.setInputProcessor(this.stage);

        //Initialize background
        //Background
		this.backgroundTexture = new Texture(Gdx.files.internal("MenuBg.png"));
        this.backgroundSupplies1Texture = new Texture(Gdx.files.internal("BackSuppliesBg.png"));
        this.backgroundSupplies2Texture = new Texture(Gdx.files.internal("FrontSuppliesBg.png"));
        this.backgroundCharacterTexture = new Texture(Gdx.files.internal("CharacterMainBg.png"));

        //Background Controllers
        bgCharacterController = new Rectangle();
        bgCharacterController.setSize(1,1);
        bgCharacterController.x = 0;
        bgCharacterController.y = -400;


        bgSupplies1Controller= new Rectangle();
        bgSupplies1Controller.setSize(1,1);
        bgSupplies1Controller.x = 0;
        bgSupplies1Controller.y = -600;

        bgSupplies2Controller= new Rectangle();
        bgSupplies2Controller.setSize(1,1);
        bgSupplies2Controller.x = 0;
        bgSupplies2Controller.y = -800;

        bgState = "enter";
        bgSin = 0.0;
        startTransp = 0.0;

        //Menu Items

        //Title
		this.titleTexture = new Texture(Gdx.files.internal("Title1.png"));
        titleAlpha = 0.0f;
        bmpFnt = new BitmapFont(Gdx.files.internal("pxlFnt.fnt"));
        bmpFnt2 = new BitmapFont(Gdx.files.internal("pxelFnt2.fnt"));

        layout = new GlyphLayout();

		//Transition Screen Effect

        batch = new SpriteBatch();
		this.transitionTexture = new Texture(Gdx.files.internal("TransitionTitle.png"));
		this.transitionBackground = new Texture(Gdx.files.internal("TransitionScreen2.png"));

		transitionController = new Rectangle();
        transition = "enter";
        transitionController.x = 0;
        transitionController.y = 0;
        transitionAlpha = 1.0f;
        transitionToNext = "";


        //Button

        this.settingsBtnTexture = new Texture(Gdx.files.internal("SettingsBtn.png"));
        this.settingsBtnPressedTexture = new Texture(Gdx.files.internal("SettingsPressedBtn.png"));

        this.settingsBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(this.settingsBtnTexture)), new TextureRegionDrawable(new TextureRegion(this.settingsBtnPressedTexture)));
        this.settingsBtn.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                if (transition == "idle"){
                    transition = "exit";
                    transitionToNext = "options";
                }

            }
        });

        this.settingsBtn.setPosition(80.0f-settingsBtn.getWidth()/2,-80.0f-settingsBtn.getHeight()/2);
        this.settingsBtn.getImage().scaleBy(3.0f);
        this.stage.addActor(this.settingsBtn);
        this.settingsBtn.addAction(Actions.sequence(Actions.delay(0.25f),Actions.moveBy(0.0f,120.0f,0.75f, Interpolation.sineOut)));
	    this.settingsBtn.setDisabled(true);
    }

	public void resize(int width, int height) {
		super.resize(width, height);
        this.stage.getViewport().update(width, height, true);
	}

	public void render(float delta) {

        super.render(delta);
        this.clearScreen();

        if (bgmStarted == false) {
            bgmsc.play(0.2f);
            bgmsc.loop();
            bgmStarted = true;
            prefs.putBoolean("bgmStarted",bgmStarted);
            prefs.flush();
        }

        ///TRANSITION

        if (transition == "exit") {
            transitionController.y -= Math.abs(0-transitionController.y/6);

            if (transitionAlpha <1.0f){
                transitionAlpha +=0.1f;

            }
            if (transitionController.y <= 10) {

                prefs = Gdx.app.getPreferences("MyPreferences");
                prefs.putInteger("prevlivesNo",3);
                prefs.putInteger("livesNo",3);
                prefs.putInteger("score", 0);
                prefs.flush();

                if (transitionToNext == "options") {
                    MainMenuScreen.this.game.setScreen(new OptionsScreen(MainMenuScreen.this.game));
                    MainMenuScreen.this.dispose();
                }
                if (transitionToNext == "game") {
                    MainMenuScreen.this.game.setScreen(new DayScreen(MainMenuScreen.this.game));
                    MainMenuScreen.this.dispose();
                }
                else{
                    return;
                }

            }
        }
		if (transition == "enter") {
			transitionController.y -= 72;


			if (transitionController.y <= -720) {
				if (transition == "enter")
					transition = "idle";
				transitionController.y = 720;

			}
		}
        if ((transitionAlpha > 0.02 ) && (transition == "enter" || transition == "idle")){
            transitionAlpha -=0.02f;
        }

        //Move the background controller up

        if (bgCharacterController.y < 0 && bgState == "enter"){
            bgCharacterController.y += Math.abs(bgCharacterController.y/12);
            bgSupplies1Controller.y += Math.abs(bgSupplies1Controller.y/12);
            bgSupplies2Controller.y += Math.abs(bgSupplies2Controller.y/12);
        }

        if (bgCharacterController.y > -14){
            if (bgState == "enter"){
                bgState = "idle";
                this.settingsBtn.setDisabled(false);
            }
        }

        if (titleAlpha < 0.98f){
            titleAlpha += 0.02f;
        }

        startTransp += 0.025;


        //SCREENSHAKE

        vs = Gdx.files.internal("3dVert.glsl").readString();
        fs = Gdx.files.internal("3dFrag.glsl").readString();

        shaderProgram.pedantic = false;
        shaderProgram = new ShaderProgram(vs,fs);
        //batch.setShader(shaderProgram);

        shaderProgram.begin();
        shaderProgram.setUniformf("u_color", MathUtils.random(1,5)/10f);
        shaderProgram.setUniformf("u_color2", MathUtils.random(1,5)/10f);
        shaderProgram.setUniformf("u_color3", MathUtils.random(1,5)/10f);
        shaderProgram.setUniformf("u_distort", MathUtils.random(4.0f),MathUtils.random(4.0f),0);
        //shaderProgram.setUniformf("stereoLeft",-1);
        //shaderProgram.setUniformf("stereoRight",-1);
        //shaderProgram.setAttributef("in_Position",-1,-1,-1,-1);


        shaderProgram.end();





        batch.begin();
        batch.setColor(1.0f,1.0f,1.0f,titleAlpha);
        batch.draw(backgroundTexture,0.0f,0.0f,xx,yy);
        batch.setColor(1.0f,1.0f,1.0f,1.0f);


        batch.draw(backgroundSupplies1Texture,0.0f,bgSupplies1Controller.y,xx,yy);
        batch.draw(backgroundSupplies2Texture,0.0f,bgSupplies2Controller.y,xx,yy);


        batch.draw(backgroundCharacterTexture,0.0f,bgCharacterController.y,xx,yy);

        batch.setColor(1.0f,1.0f,1.0f,titleAlpha);
        batch.draw(titleTexture,(xx/2)-(titleTexture.getWidth()*(xx/320)/2),(yy/6)-(titleTexture.getHeight()*(xx/320)/2),titleTexture.getWidth()*(xx/320),titleTexture.getHeight()*(xx/320));
        batch.setColor(0.0f,0.0f,0.0f,1.0f);


            float transp = (float) Math.floor(startTransp%2);

            if (startTransp > 11){
                startTransp = 11.0;
            }
            bmpFnt.getData().setScale(xx/1280);
            bmpFnt.setColor(0.0f,0.0f,0.0f,transp);
            bmpFnt.draw(batch, "TAP TO START", xx * 19 / 30, yy / 4 + 5, 1.0f, 1, false);
            bmpFnt.setColor(1.0f, 1.0f, 1.0f, transp);
            bmpFnt.draw(batch, "TAP TO START", xx * 19 / 30, yy / 4 + 10, 1.0f, 1, false);

        bmpFnt.setColor(1.0f,1.0f,1.0f,1.f);
        bmpFnt.draw(batch, "HIGH SCORE", xx/25, yy*0.95f, 1.0f, Align.left, false);
        bmpFnt2.getData().setScale(xx/1280);
        bmpFnt2.draw(batch,String.valueOf(score),xx/25,yy*0.9f,1.0f, Align.left, false);

        batch.end();

        this.stage.act(delta);
        this.stage.draw();

        batch.begin();
        batch.setColor(1.0f,1.0f,1.0f, transitionAlpha);
        batch.draw(transitionBackground,0.0f,0.0f,xx,yy);
        batch.setColor(1.0f,1.0f,1.0f,1.0f);
        batch.draw(transitionTexture,transitionController.x,transitionController.y,xx,yy);
        batch.end();

        if (Gdx.input.isTouched()) {

            if (Gdx.input.getX() > xx/5 && Gdx.input.getX() < xx * 5/6) {

                if (transition == "idle") {
                    transitionToNext = "game";
                    transition = "exit";
                }

            }
        }



    }

	public void dispose() {
		super.dispose();
		this.stage.dispose();

		this.backgroundTexture.dispose();
		this.backgroundSupplies1Texture.dispose();
		this.backgroundSupplies2Texture.dispose();
		this.backgroundCharacterTexture.dispose();

		this.titleTexture.dispose();
		this.settingsBtnTexture.dispose();
		this.settingsBtnPressedTexture.dispose();


	    this.transitionTexture.dispose();
	    this.transitionBackground.dispose();

	}

	private void clearScreen() {
		Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
		Gdx.gl.glClear(16384);
	}
}
