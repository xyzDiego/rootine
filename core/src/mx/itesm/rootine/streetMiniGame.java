package mx.itesm.rootine;

/**
 * Created by prez on 2/25/18.
 */

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.util.Iterator;

public class streetMiniGame extends ScreenAdapter {

    /////////IMPORTANT OBJECTS NEEDED IN EVERY MINIGAME /////
    //START

    //Important Variables
    private final Game game;
    private float xx = Gdx.graphics.getWidth();
    private float yy = Gdx.graphics.getHeight();


    //Level Stage, Background
    private Stage stage;
    private Texture bg;
    private Texture blk;
    private SpriteBatch batch;


    //Controllers (Variables/objects that determine time or movement)

    private Rectangle failWinController;
    private float pausedAlpha;


    //Text & Fonts
    BitmapFont bmpFnt;
    GlyphLayout layout;


    //UI
    private Texture Failed;
    private Texture Win;
    private Texture FailedSym;
    private Texture WinSym;
    private Texture pauseBtnTexture;
    private Texture pauseBtnPressedTexture;
    private ImageButton pauseBtn;
    private Texture pausedTitle;
    private int alarm = 120;



    private Texture resumeBtnTexture;
    private Texture resumeBtnPressedTexture;
    private ImageButton resumeBtn;

    private Texture menuBtnTexture;
    private Texture menuBtnPressedTexture;
    private ImageButton menuBtn;


    //Transition Elements
    private Texture transitionTexture;
    private Texture transitionBackground;
    private String transition;
    private Rectangle transitionController;
    private Float transitionAlpha;

    //MiniGame Variables
    private Texture time;
    private int screenTime;
    private int noMiniGames;
    private String gameState;
    private int instructionPos = -50;
    private Float instructionAlpha;
    private int points;
    private int startTrans = 50;



    private Array<Rectangle> CarsOne;
    private Texture CarOneText;
    private long lastCarOne;


    private Array<Rectangle> CarsTwo;
    private Texture CarTwoText;
    private long lastCarTwo;

    private static final int SHEET_COLUMNS = 4, SHEET_ROWS = 1;
    private Animation<TextureRegion> guyAnim;
    private Texture guySheet;
    private SpriteBatch spriteBatch;
    private Integer move;
    private Integer fling;
    float stateTime;

    private Sound win;
    private Sound loss;
    private boolean fanfarePlay = false;



    //Shader Variables


    //END


    /////////MINIGAME DEPENDENT OBJECTS/////


    //Level Objects
    private Boolean paused = false;

    //Level Specific Variables
    private float gameSpeed;

    private Rectangle playerRect;



    public streetMiniGame(Game game) {
        this.game = game;
    }

    public void show() {

        super.show();

        //CHECK PREFERENCES, GET AND SET VALUES
        Preferences prefs = Gdx.app.getPreferences("MyPreferences");
        noMiniGames = prefs.getInteger("minigamesNo",0);
        noMiniGames +=1;
        prefs.putInteger("minigamesNo",noMiniGames);
        gameSpeed = prefs.getFloat("gameSpeed",2);
        prefs.flush();

        //DEFINE THE STAGE & SPRITEBATCH
        this.stage = new Stage(new FitViewport(1280.0F, 720.0F));
        Gdx.input.setInputProcessor(this.stage);
        batch = new SpriteBatch();


        //FOR TRANSITIONS
        transitionController = new Rectangle();
        transition = "enter";
        transitionController.x = 0;
        transitionController.y = 0;
        transitionAlpha = 1.0f;
        instructionAlpha = 1.0f;
        this.transitionTexture = new Texture(Gdx.files.internal("TransitionTitle.png"));
        this.transitionBackground = new Texture(Gdx.files.internal("TransitionScreen2.png"));

        //Guy Animation
        guySheet = new Texture(Gdx.files.internal("dude_strip4.png"));

        //Divide la spritesheet en una matriz con las diferentes texturas
        TextureRegion[][] temp = TextureRegion.split(guySheet, guySheet.getWidth()/SHEET_COLUMNS,
                guySheet.getHeight()/SHEET_ROWS);

        //Acomoda los sprites en un array para poder iterarlos
        TextureRegion[] spriteFrames = new TextureRegion[SHEET_COLUMNS*SHEET_ROWS];
        int index = 0;
        for (int i = 0; i < SHEET_ROWS; i++){
            for (int j = 0; j < SHEET_COLUMNS; j++){
                spriteFrames[index++] = temp[i][j];
            }
        }
        guyAnim = new Animation<TextureRegion>(0.15f, spriteFrames);
        stateTime = 0f;
        this.playerRect = new Rectangle();
        playerRect.x = xx/2;
        playerRect.y = yy/2;
        playerRect.setSize(guySheet.getWidth()/SHEET_COLUMNS*MathUtils.round(xx/427), guySheet.getHeight()*MathUtils.round(xx/427)/3);


        //MINIGAME TIME & STATES, FAIL WINE
        this.time = new Texture(Gdx.files.internal("timeBar.png"));
        this.bg = new Texture(Gdx.files.internal("street.png"));
        this.Failed = new Texture(Gdx.files.internal("failedTxt.png"));
        this.Win = new Texture(Gdx.files.internal("AwesomeTxt.png"));
        this.FailedSym = new Texture(Gdx.files.internal("FailedSymbol.png"));
        this.WinSym = new Texture(Gdx.files.internal("AwesomeSymbol.png"));
        this.blk = new Texture(Gdx.files.internal("blk.png"));
        this.CarOneText = new Texture(Gdx.files.internal("car2.png"));
        this.CarTwoText = new Texture(Gdx.files.internal("car1.png"));

        this.win = Gdx.audio.newSound(Gdx.files.internal("win.wav"));
        win.play(0.f);

        this.loss = Gdx.audio.newSound(Gdx.files.internal("lifelose.wav"));
        loss.play(0.f);



        gameState = "running";
        failWinController = new Rectangle();
        failWinController.setSize(1,1);
        failWinController.x = 0;
        failWinController.y = -600;


        pausedAlpha = 0.0f;

        //FONT(S)
        bmpFnt = new BitmapFont(Gdx.files.internal("pxelFnt2.fnt"));
        layout = new GlyphLayout();

        points = prefs.getInteger("score",0);
        screenTime = 250;
        move = 0;
        fling = 0;




        //Pause Button

        this.pauseBtnTexture = new Texture(Gdx.files.internal("pauseBtn.png"));
        this.pauseBtnPressedTexture = new Texture(Gdx.files.internal("pauseBtnPressed.png"));
        this.pausedTitle = new Texture(Gdx.files.internal("PausedTitle.png"));


        this.pauseBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(this.pauseBtnTexture)), new TextureRegionDrawable(new TextureRegion(this.pauseBtnPressedTexture)));
        this.pauseBtn.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                if (transition == "idle" && screenTime > 0)
                    paused = !paused;

            }
        });

        this.pauseBtn.setPosition(stage.getWidth()-80.0f-pauseBtn.getWidth()/2,-80.0f-pauseBtn.getHeight()/2);
        this.pauseBtn.getImage().scaleBy(2.0f);
        this.stage.addActor(this.pauseBtn);
        this.pauseBtn.addAction(Actions.sequence(Actions.delay(0.25f),Actions.moveBy(0.0f,120.0f,0.75f, Interpolation.sineOut)));


        //Resume Button

        this.resumeBtnTexture = new Texture(Gdx.files.internal("resumeBtn.png"));
        this.resumeBtnPressedTexture = new Texture(Gdx.files.internal("resumePressedBtn.png"));


        this.resumeBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(this.resumeBtnTexture)), new TextureRegionDrawable(new TextureRegion(this.resumeBtnPressedTexture)));
        this.resumeBtn.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                if (transition == "idle" && screenTime > 0 && paused)
                    paused = false;

            }
        });

        this.resumeBtn.getImage().scaleBy(3.0f);
        this.resumeBtn.setPosition(stage.getWidth()/2 -(resumeBtn.getWidth()*3/1.5f),stage.getHeight()/2-resumeBtn.getHeight()/2);
        this.stage.addActor(this.resumeBtn);
        resumeBtn.setDisabled(true);
        resumeBtn.setColor(1.0f,1.0f,1.0f,0.0f);

        //Menu Button

        this.menuBtnTexture = new Texture(Gdx.files.internal("menuBtn.png"));
        this.menuBtnPressedTexture = new Texture(Gdx.files.internal("menuPressedBtn.png"));


        this.menuBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(this.menuBtnTexture)), new TextureRegionDrawable(new TextureRegion(this.menuBtnPressedTexture)));
        this.menuBtn.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                if (transition == "idle" && screenTime > 0)
                    paused = !paused;

            }
        });

        this.menuBtn.getImage().scaleBy(3.0f);
        this.menuBtn.setPosition(stage.getWidth()/2 -(menuBtn.getWidth()*3/1.5f),stage.getHeight()/4-menuBtn.getHeight()/2);
        this.stage.addActor(this.menuBtn);
        menuBtn.setDisabled(true);
        menuBtn.setColor(1.0f,1.0f,1.0f,0.0f);


        //SHADER

        CarsOne = new Array<Rectangle>();
        sendCarOne();

        CarsTwo = new Array<Rectangle>();
        sendCarTwo();

    }

    public void resize(int width, int height) {
        super.resize(width, height);
        this.stage.getViewport().update(width, height, true);
    }

    public void render(float delta) {

        super.render(delta);
        this.clearScreen();

        //FOR TRANSITIONS
        if (transition == "exit") {
            transitionController.y -= Math.abs(0-transitionController.y/6);
            if (transitionAlpha <1.0f){
                transitionAlpha +=0.1f;

            }

            if (transitionController.y <= 10) {
                Preferences prefs = Gdx.app.getPreferences("MyPreferences");
                if (gameState == "win") {
                    prefs.putInteger("addPoints", 100);
                }if (gameState == "failed") {
                    prefs.putInteger("addPoints", 0);
                }
                prefs.flush();
                streetMiniGame.this.game.setScreen(new PointsScreen(streetMiniGame.this.game));
                streetMiniGame.this.dispose();
            }
        }

        if (transition == "enter") {
            transitionController.y -= 72;

            if (transitionController.y <= -720) {
                if (transition == "enter")
                    transition = "idle";
                transitionController.y = 720;
            }
        }

        if ((transitionAlpha > 0.02) && (transition == "enter" || transition == "idle")){
            transitionAlpha -=0.02f;
        }
        if (screenTime < 150 && instructionAlpha > 0.02){
            instructionAlpha -=0.02f;
        }

        if (!paused) {
            //FAIL WIN CONTROLLERS
            screenTime -= gameSpeed/3;
            if (screenTime <= 0) {
                if (gameState != "win") {
                    gameState = "failed";
                }
            }
            if (TimeUtils.nanoTime() - lastCarOne > 21*100000000) {
                sendCarOne();
            }
            Iterator<Rectangle> iter = CarsOne.iterator();
            while (iter.hasNext()){
                Rectangle carr = iter.next();
                carr.x += xx/3 * Gdx.graphics.getDeltaTime();

                if (carr.getX() > xx*1.1f){
                    iter.remove();
                }
                if (carr.overlaps(playerRect)){
                    if (gameState == "running") {
                        gameState = "failed";
                        fling = 60;
                        screenTime = 0;
                    }

                }
            }
            if (TimeUtils.nanoTime() - lastCarTwo > 13*100000000) {
                sendCarTwo();
            }


            Iterator<Rectangle> iter2 = CarsTwo.iterator();

            while (iter2.hasNext()){
                Rectangle carr2 = iter2.next();
                carr2.x -= xx/2 * Gdx.graphics.getDeltaTime();

                if (carr2.getX() <-xx*0.2f){
                    iter2.remove();
                }
                if (carr2.overlaps(playerRect)){
                    if (gameState == "running") {
                        gameState = "failed";
                        fling = -60;
                        screenTime = 0;
                    }
                }
            }

            if (Gdx.input.isTouched() && move == 0 && gameState == "running"){
                move = Math.round(yy/50);
            }
            if (move >0 && gameState == "running"){
                playerRect.y-=move;
                move -=1;
            }
            if (fling != 0 && gameState == "failed"){
                playerRect.x+=fling;
                if (fling < 0){
                    fling +=1;
                } else {
                    fling -=1;
                }
            }
            if (playerRect.y <= -yy*.1 && gameState == "running"){
                gameState = "win";
                screenTime = 0;
            }


            stateTime += Gdx.graphics.getDeltaTime();


            if (instructionPos < 80 && transition == "idle")   //MOVE THE MINIGAME INSTRUCTION INTO SCREEN
                instructionPos += Math.abs((80 - instructionPos) / 16);


            if (screenTime <= 0 && startTrans <= 0) //MOVE THE FAIL/WIN TEXT INTO SCREEN
                failWinController.y += Math.abs(failWinController.y / 12);



        }
        if (alarm > 0 && screenTime <= 0) {
            alarm -= 1;
        } else if (alarm == 0 && screenTime <= 0) {
            if (transition != "exit")
                transition = "exit";
        }
        if (screenTime <= 0){
            paused = false;
            this.pauseBtn.setDisabled(true);
            this.pauseBtn.addAction(Actions.sequence(Actions.moveBy(0.0f, -120.0f, 0.75f, Interpolation.sineOut)));
        }
        if (screenTime <= 0 && fanfarePlay == false){
            if (gameState == "win"){
                win.play(1.0f);

            }
            if (gameState == "failed"){
                loss.play(1.0f);
            }
            fanfarePlay = true;

        }

        //DRAW ONLY!
        //DRAW BACKGROUND
        batch.begin();
        batch.draw(bg,0.0f,0.0f,xx,yy);
        batch.end();

        for (Rectangle CarsOne : CarsOne) {
            batch.begin();
            batch.draw(CarOneText, CarsOne.x, CarsOne.y, CarOneText.getWidth()*(Math.round(xx/427)), CarOneText.getHeight()*(Math.round(xx/427)));
            batch.end();

        }
        for (Rectangle CarsTwo : CarsTwo) {
            batch.begin();
            batch.draw(CarTwoText, CarsTwo.x, CarsTwo.y, CarTwoText.getWidth()*(Math.round(xx/427)), CarTwoText.getHeight()*(Math.round(xx/427)));
            batch.end();

        }
        TextureRegion currentFrame = guyAnim.getKeyFrame(stateTime, true);
        batch.begin();
        batch.draw(currentFrame, playerRect.x,playerRect.y, currentFrame.getRegionWidth() * (Math.round(xx / 427)), currentFrame.getRegionHeight() * (Math.round(xx / 427)));
        batch.end();

        this.stage.act(delta);
        this.stage.draw();


         batch.begin();
        batch.draw(time,0,yy-yy/36,screenTime*(xx/250),yy/36);
        bmpFnt.getData().setScale(xx/1280);


        bmpFnt.setColor(1.0f,1.0f,1.0f,instructionAlpha);
        bmpFnt.getData().setScale(xx/640);
        bmpFnt.draw(batch,"CROSS",instructionPos,yy*9/10);
        bmpFnt.getData().setScale(xx/1280);
        bmpFnt.draw(batch,"THE STREET",instructionPos,yy*9/10 - (yy*.097f));
        bmpFnt.setColor(1.0f,1.0f,1.0f,1.0f);


        if (paused){

            stage.addActor(resumeBtn);
            resumeBtn.setDisabled(false);
            resumeBtn.setColor(1.0f,1.0f,1.0f,1.0f);
            stage.addActor(menuBtn);
            menuBtn.setDisabled(false);
            menuBtn.setColor(1.0f,1.0f,1.0f,1.0f);


            if (pausedAlpha < 0.8f){
                pausedAlpha += 0.05f;
            }

        } else {

            resumeBtn.setDisabled(true);
            resumeBtn.remove();

            resumeBtn.setColor(1.0f,1.0f,1.0f,0.0f);

            menuBtn.setDisabled(true);
            menuBtn.remove();
            menuBtn.setColor(1.0f,1.0f,1.0f,0.0f);

            if (pausedAlpha > 0.1f){
                pausedAlpha -= 0.1f;
            }
        }

        batch.setColor(1.0f,1.0f,1.0f, pausedAlpha);
        batch.draw(blk,0,0,xx,yy);
        batch.setColor(1.0f,1.0f,1.0f,1.0f);
        batch.draw(pausedTitle,xx/2-pausedTitle.getWidth()*(Math.round(xx / 320)) / 2, (yy*1.05f)-(pausedAlpha*yy*0.4f),pausedTitle.getWidth()*(Math.round(xx / 320)),pausedTitle.getHeight()*(Math.round(xx / 320)));
        batch.end();

        if (startTrans > 0) {
            startTrans -= 1;
        } else if (startTrans <= 0) {
            batch.begin();
            if (gameState == "failed") {
                batch.draw(FailedSym, 0.0f,Math.abs(failWinController.y), xx, yy);
                batch.draw(Failed, 0.0f, failWinController.y, xx, yy);
            }
            if (gameState == "win") {
                batch.draw(WinSym, 0.0f, Math.abs(failWinController.y), xx, yy);
                batch.draw(Win, 0.0f, failWinController.y, xx, yy);
            }
            batch.end();
        }

        batch.begin();
        batch.setColor(1.0f,1.0f,1.0f, transitionAlpha);
        batch.draw(transitionBackground,0.0f,0.0f,xx,yy);
        batch.setColor(1.0f,1.0f,1.0f,1.0f);
        batch.draw(transitionTexture,transitionController.x,transitionController.y,xx,yy);

        batch.end();
    }

    public void dispose() {
        super.dispose();
        this.stage.dispose();

    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(16384);
    }
    private void sendCarOne() {
        Rectangle car = new Rectangle();
        car.x = -xx/2;
        car.y = yy/3;
        car.width = CarOneText.getWidth()*MathUtils.round(xx/427);
        car.height = CarOneText.getHeight()*MathUtils.round(xx/427);
        this.CarsOne.add(car);
        lastCarOne = TimeUtils.nanoTime();
    }
    private void sendCarTwo() {
        Rectangle car = new Rectangle();
        car.x = xx*1.1f;
        car.y = yy/6;
        car.width = CarOneText.getWidth()*MathUtils.round(xx/427);
        car.height = CarOneText.getHeight()*MathUtils.round(xx/427);
        this.CarsTwo.add(car);
        lastCarTwo = TimeUtils.nanoTime();
    }
}
