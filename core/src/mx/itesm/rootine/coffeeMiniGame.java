package mx.itesm.rootine;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.util.Iterator;

/**
 * Created by Ian on 04/04/2018.
 */

public class coffeeMiniGame extends ScreenAdapter{

    private final Game game;
    private float xx = Gdx.graphics.getWidth();
    private float yy = Gdx.graphics.getHeight();
    private Stage stage;
    private Texture backgroundTexture;
    private Texture bg;
    private Texture time;
    private Texture coffee;

    private Texture cafeteraImg, tazaImg;

    private Rectangle Cafetera;
    private Rectangle Taza;
    private Array<Rectangle> Cafe;

    //UI
    private Texture Failed;
    private Texture Win;
    private Texture FailedSym;
    private Texture WinSym;
    private Texture pauseBtnTexture;
    private Texture pauseBtnPressedTexture;
    private ImageButton pauseBtn;
    private Texture pausedTitle;
    private Texture blk;
    private int alarm = 120;



    private Texture resumeBtnTexture;
    private Texture resumeBtnPressedTexture;
    private ImageButton resumeBtn;

    private Texture menuBtnTexture;
    private Texture menuBtnPressedTexture;
    private ImageButton menuBtn;

    private Rectangle failWinController;
    private float pausedAlpha;
    private int startTrans = 50;



    BitmapFont bmpFnt;
    GlyphLayout layout;

    private double maxSpd;
    private double movSpd;
    private long lastDropTime;

    private Texture transitionTexture;
    private Texture transitionBackground;
    private String transition;
    private Rectangle transitionController;
    private SpriteBatch batch;

    private Boolean paused = false;

    private Float transitionAlpha;

    private int screenTime;
    private int noMiniGames;
    private int noDays;
    private String gameState;
    private float gameSpeed;
    private int dir = 0;
    private int progress = 0;
    private int instructionPos = -50;
    private Float instructionAlpha;
    private int points;

    private Sound win;
    private Sound loss;
    private boolean fanfarePlay = false;
    private boolean playCoffee = true;


    private Sound coffesnd;

    public coffeeMiniGame(Game game) {
        this.game = game;
    }

    public void show() {

        super.show();


        Preferences prefs = Gdx.app.getPreferences("MyPreferences");
        noMiniGames = prefs.getInteger("minigamesNo", 0);
        noMiniGames += 1;
        prefs.putInteger("minigamesNo", noMiniGames);
        gameSpeed = prefs.getFloat("gameSpeed",2);
        prefs.flush();

        this.stage = new Stage(new FitViewport(1280.0F, 720.0F));
        Gdx.input.setInputProcessor(this.stage);
        this.backgroundTexture = new Texture(Gdx.files.internal("OptionsBg.png"));
        Image background = new Image(this.backgroundTexture);


        bmpFnt = new BitmapFont(Gdx.files.internal("pxelFnt2.fnt"));
        layout = new GlyphLayout();

        batch = new SpriteBatch();
        this.transitionTexture = new Texture(Gdx.files.internal("TransitionTitle.png"));
        this.transitionBackground = new Texture(Gdx.files.internal("TransitionScreen2.png"));
        this.time = new Texture(Gdx.files.internal("timeBar.png"));
        this.bg = new Texture(Gdx.files.internal("m_cafeBG.png"));

        this.Failed = new Texture(Gdx.files.internal("failedTxt.png"));
        this.Win = new Texture(Gdx.files.internal("AwesomeTxt.png"));
        this.FailedSym = new Texture(Gdx.files.internal("FailedSymbol.png"));
        this.WinSym = new Texture(Gdx.files.internal("AwesomeSymbol.png"));
        this.blk = new Texture(Gdx.files.internal("blk.png"));


        transitionController = new Rectangle();
        transition = "enter";
        transitionController.x = 0;
        transitionController.y = 0;
        transitionAlpha = 1.0f;
        instructionAlpha = 1.0f;

        gameState = "running";
        failWinController = new Rectangle();
        failWinController.setSize(1,1);
        failWinController.x = 0;
        failWinController.y = -600;


        points = prefs.getInteger("score",0);
        screenTime = 250;

        batch = new SpriteBatch();



        cafeteraImg = new Texture(Gdx.files.internal("Cafetera.png"));
        tazaImg = new Texture(Gdx.files.internal("taza.png"));
        coffee = new Texture(Gdx.files.internal("coffee.png"));

        this.Taza = new Rectangle();
        Taza.x = xx/ 2 - tazaImg.getWidth()*(xx/427)/ 2;
        Taza.y = yy/4;
        Taza.width = tazaImg.getWidth()*(xx/427)*0.75f;
        Taza.height = (tazaImg.getHeight())*(xx/427)/2;

        this.Cafetera = new Rectangle();
        Cafetera.x = MathUtils.random(xx*.01f,xx*0.75f);
        Cafetera.y = yy*0.667f;


        movSpd = 0;
        maxSpd = 100;

        Cafe = new Array<Rectangle>();
        dropCafe();

        this.win = Gdx.audio.newSound(Gdx.files.internal("win.wav"));
        win.play(0.f);

        this.loss = Gdx.audio.newSound(Gdx.files.internal("lifelose.wav"));
        loss.play(0.f);

        this.coffesnd = Gdx.audio.newSound(Gdx.files.internal("sCoffee.wav"));
        coffesnd.play(0.f);

        //Pause Button

        this.pauseBtnTexture = new Texture(Gdx.files.internal("pauseBtn.png"));
        this.pauseBtnPressedTexture = new Texture(Gdx.files.internal("pauseBtnPressed.png"));
        this.pausedTitle = new Texture(Gdx.files.internal("PausedTitle.png"));


        this.pauseBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(this.pauseBtnTexture)), new TextureRegionDrawable(new TextureRegion(this.pauseBtnPressedTexture)));
        this.pauseBtn.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                if (transition == "idle" && screenTime > 0)
                    paused = !paused;

            }
        });

        this.pauseBtn.setPosition(stage.getWidth()-80.0f-pauseBtn.getWidth()/2,-80.0f-pauseBtn.getHeight()/2);
        this.pauseBtn.getImage().scaleBy(2.0f);
        this.stage.addActor(this.pauseBtn);
        this.pauseBtn.addAction(Actions.sequence(Actions.delay(0.25f),Actions.moveBy(0.0f,120.0f,0.75f, Interpolation.sineOut)));


        //Resume Button

        this.resumeBtnTexture = new Texture(Gdx.files.internal("resumeBtn.png"));
        this.resumeBtnPressedTexture = new Texture(Gdx.files.internal("resumePressedBtn.png"));


        this.resumeBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(this.resumeBtnTexture)), new TextureRegionDrawable(new TextureRegion(this.resumeBtnPressedTexture)));
        this.resumeBtn.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                if (transition == "idle" && screenTime > 0&& paused)
                    paused = false;

            }
        });

        this.resumeBtn.getImage().scaleBy(3.0f);
        this.resumeBtn.setPosition(stage.getWidth()/2 -(resumeBtn.getWidth()*3/1.5f),stage.getHeight()/2-resumeBtn.getHeight()/2);
        this.stage.addActor(this.resumeBtn);
        resumeBtn.setDisabled(true);
        resumeBtn.setColor(1.0f,1.0f,1.0f,0.0f);

        //Menu Button

        this.menuBtnTexture = new Texture(Gdx.files.internal("menuBtn.png"));
        this.menuBtnPressedTexture = new Texture(Gdx.files.internal("menuPressedBtn.png"));


        this.menuBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(this.menuBtnTexture)), new TextureRegionDrawable(new TextureRegion(this.menuBtnPressedTexture)));
        this.menuBtn.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                if (transition == "idle" && screenTime > 0) {
                    coffeeMiniGame.this.game.setScreen(new MainMenuScreen(coffeeMiniGame.this.game));
                    coffeeMiniGame.this.dispose();
                }

            }
        });

        this.menuBtn.getImage().scaleBy(3.0f);
        this.menuBtn.setPosition(stage.getWidth()/2 -(menuBtn.getWidth()*3/1.5f),stage.getHeight()/4-menuBtn.getHeight()/2);
        this.stage.addActor(this.menuBtn);
        menuBtn.setDisabled(true);
        menuBtn.setColor(1.0f,1.0f,1.0f,0.0f);

    }

    public void resize(int width, int height) {
        super.resize(width, height);
        this.stage.getViewport().update(width, height, true);
    }

    public void render(float delta) {

        super.render(delta);
        this.clearScreen();


        if (transition == "exit") {
            transitionController.y -= Math.abs(0 - transitionController.y / 6);
            if (transitionAlpha < 1.0f) {
                transitionAlpha += 0.1f;

            }
            if (transitionController.y <= 10) {
                Preferences prefs = Gdx.app.getPreferences("MyPreferences");
                if (gameState == "win") {
                    prefs.putInteger("addPoints", 100);
                }if (gameState == "failed") {
                    prefs.putInteger("addPoints", 0);
                }
                prefs.flush();
                coffeeMiniGame.this.game.setScreen(new PointsScreen(coffeeMiniGame.this.game));
                coffeeMiniGame.this.dispose();
            }
        }
        if (transition == "enter") {
            transitionController.y -= 72;

            if (transitionController.y <= -720) {
                if (transition == "enter")
                    transition = "idle";
                transitionController.y = 720;
            }
        }

        if ((transitionAlpha > 0.02) && (transition == "enter" || transition == "idle")) {
            transitionAlpha -= 0.02f;
        }

        batch.begin();
        batch.draw(bg, 0.0f, 0.0f, xx, yy);
        batch.end();

        if (!paused) {

            screenTime -= gameSpeed/4;

            if (progress >= 1 && playCoffee){
                coffesnd.play(1.0f);
                playCoffee = false;
            }

            if (screenTime <= 0) {
                if (gameState != "win") {
                    gameState = "failed";
                }
            }

            if (progress >= 50) {
                gameState = "win";
                screenTime = 0;
            }

            if (instructionPos < 80) {
                instructionPos += Math.abs((80 - instructionPos) / 16);
            }

            for (Rectangle Cafe : Cafe) {
                batch.begin();
                batch.draw(coffee, Cafe.x, Cafe.y, coffee.getWidth() * 2, coffee.getHeight() * 2);
                batch.end();

            }
            if (TimeUtils.nanoTime() - lastDropTime > 10000000 && progress < 100) {
                dropCafe();
            }

            Iterator<Rectangle> iter = Cafe.iterator();
            while (iter.hasNext()){
                Rectangle coffeeDrop = iter.next();
                coffeeDrop.y -= (xx/2) * Gdx.graphics.getDeltaTime();
                if (coffeeDrop.y+64 < 0) {
                    iter.remove();
                }

                if (coffeeDrop.overlaps(Taza)){

                    if ( gameState == "running")
                        progress+=1;
                    iter.remove();
                }
            }

            if (gameState == "running") {
                if (Gdx.input.isTouched()) {

                    if (Gdx.input.getX() < xx/2) {
                        Taza.x -= xx/100;
                    }

                    if (Gdx.input.getX() >= xx/2) {
                        Taza.x += xx/100;

                    }
                }
                if (gameState == "running") {
                    if (Taza.x < 0)
                        Taza.x = 0;

                    if (Taza.x > xx*.99f) {
                        Taza.x = xx*.99f;
                    }
                }
            }

            if (instructionPos < 80 && transition == "idle")   //MOVE THE MINIGAME INSTRUCTION INTO SCREEN
                instructionPos += Math.abs((80 - instructionPos) / 16);
            if (screenTime < 150 && instructionAlpha > 0.02){
                instructionAlpha -=0.02f;
            }

            if (screenTime <= 0 && startTrans <= 0) //MOVE THE FAIL/WIN TEXT INTO SCREEN
                failWinController.y += Math.abs(failWinController.y / 12);

            if (alarm > 0 && screenTime <= 0) {
                alarm -= 1;
            } else if (alarm == 0 && screenTime <= 0) {
                if (transition != "exit")
                    transition = "exit";
            }
            if (screenTime <= 0){
                paused = false;
                this.pauseBtn.setDisabled(true);
                this.pauseBtn.addAction(Actions.sequence(Actions.moveBy(0.0f, -120.0f, 0.75f, Interpolation.sineOut)));
            }

        }

        if (screenTime <= 0 && fanfarePlay == false){
            if (gameState == "win"){
                win.play(1.0f);

            }
            if (gameState == "failed"){
                loss.play(1.0f);

            }
            fanfarePlay = true;

        }

        batch.begin();
        batch.draw(tazaImg, Taza.x, Taza.y,tazaImg.getWidth()*(xx/427),tazaImg.getHeight()*(xx/427));
        batch.draw(cafeteraImg, Cafetera.x, Cafetera.y,cafeteraImg.getWidth()*(xx/427),cafeteraImg.getHeight()*(xx/427));
        batch.draw(time,0,yy-yy/36,screenTime*(xx/250),yy/36);
        bmpFnt.getData().setScale(xx/640);

        bmpFnt.setColor(1.0f,1.0f,1.0f,instructionAlpha);
        bmpFnt.draw(batch,"SERVE",instructionPos,yy*9/10);
        bmpFnt.getData().setScale(xx/1280);
        bmpFnt.draw(batch,"YOUR COFFEE",instructionPos,yy*9/10 - (yy*.097f));
        bmpFnt.setColor(1.0f,1.0f,1.0f,1.0f);
        batch.end();

        this.stage.act(delta);
        this.stage.draw();


        if (paused){
            stage.addActor(resumeBtn);
            resumeBtn.setDisabled(false);
            resumeBtn.setColor(1.0f,1.0f,1.0f,1.0f);
            stage.addActor(menuBtn);
            menuBtn.setDisabled(false);
            menuBtn.setColor(1.0f,1.0f,1.0f,1.0f);



            if (pausedAlpha < 0.8f){
                pausedAlpha += 0.05f;
            }

        } else {
            resumeBtn.setDisabled(true);
            resumeBtn.remove();

            resumeBtn.setColor(1.0f,1.0f,1.0f,0.0f);

            menuBtn.setDisabled(true);
            menuBtn.remove();
            menuBtn.setColor(1.0f,1.0f,1.0f,0.0f);


            if (pausedAlpha > 0.1f){
                pausedAlpha -= 0.1f;
            }
        }
        batch.begin();
        batch.setColor(1.0f,1.0f,1.0f, pausedAlpha);
        batch.draw(blk,0,0,xx,yy);
        batch.setColor(1.0f,1.0f,1.0f,1.0f);
        batch.draw(pausedTitle,xx/2-pausedTitle.getWidth()*(Math.round(xx / 320)) / 2, (yy*1.05f)-(pausedAlpha*yy*0.4f),pausedTitle.getWidth()*(Math.round(xx / 320)),pausedTitle.getHeight()*(Math.round(xx / 320)));
        batch.end();

        if (!paused) {
            if (startTrans > 0) {
                startTrans -= 1;
            } else if (startTrans <= 0) {
                batch.begin();
                if (gameState == "failed") {
                    batch.draw(FailedSym, 0.0f, Math.abs(failWinController.y), xx, yy);
                    batch.draw(Failed, 0.0f, failWinController.y, xx, yy);
                }
                if (gameState == "win") {
                    batch.draw(WinSym, 0.0f, Math.abs(failWinController.y), xx, yy);
                    batch.draw(Win, 0.0f, failWinController.y, xx, yy);
                }
                batch.end();
            }
        }



        batch.begin();
        batch.setColor(1.0f,1.0f,1.0f, transitionAlpha);
        batch.draw(transitionBackground,0.0f,0.0f,xx,yy);
        batch.setColor(1.0f,1.0f,1.0f,1.0f);
        batch.draw(transitionTexture,transitionController.x,transitionController.y,xx,yy);
        batch.end();
    }

    public void dispose() {
        super.dispose();
        this.stage.dispose();
        this.backgroundTexture.dispose();
    }

    private void dropCafe() {
        Rectangle Cafe = new Rectangle();
        Cafe.x = Cafetera.x+MathUtils.random(-3,3);
        Cafe.y = Cafetera.y+(yy/5.34f);
        Cafe.width = xx/20;
        Cafe.height = xx/20;
        this.Cafe.add(Cafe);
        lastDropTime = TimeUtils.nanoTime();
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(16384);
    }
}

