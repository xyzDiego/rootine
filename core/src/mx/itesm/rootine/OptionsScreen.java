package mx.itesm.rootine;

/**
 * Created by prez on 2/25/18.
 */

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class OptionsScreen extends ScreenAdapter {
    private final Game game;
    private float xx = Gdx.graphics.getWidth();
    private float yy = Gdx.graphics.getHeight();
    private Stage stage;

    private Texture backgroundTexture;

    private Table table;
    private ImageButton backButton;
    private Texture backTexture;
    private Texture backPressedTexture;
    private Texture titleTexture;


    private ImageButton SFXButton;
    private Texture SFXTexture;
    private Texture SFXPressedTexture;

    private ImageButton SoundButton;
    private Texture SoundTexture;
    private Texture SoundPressedTexture;

    BitmapFont bmpFnt;
    GlyphLayout layout;



    private Texture transitionTexture;
    private Texture transitionBackground;
    private String transition;
    private Rectangle transitionController;
    private SpriteBatch batch;

    private Float transitionAlpha;

    private String txt;
    private String txt2;
    private double i;
    private Boolean soundEna;
    private Boolean musicEna;

    private Sound snd;


    public OptionsScreen(Game game) {
        this.game = game;
    }

    public void show() {

        super.show();

        Preferences prefs = Gdx.app.getPreferences("MyPreferences");
        soundEna = prefs.getBoolean("soundena",true);
        musicEna = prefs.getBoolean("musicena",true);
        prefs.flush();


        this.stage = new Stage(new FitViewport(1280.0F, 720.0F));
        Gdx.input.setInputProcessor(this.stage);
        this.backgroundTexture = new Texture(Gdx.files.internal("OptionsBg.png"));
        Image background = new Image(this.backgroundTexture);

        batch = new SpriteBatch();
        this.transitionTexture = new Texture(Gdx.files.internal("TransitionTitle.png"));
        this.transitionBackground = new Texture(Gdx.files.internal("TransitionScreen2.png"));


        Sound snd = Gdx.audio.newSound(Gdx.files.internal("gun.mp3"));
        snd.play(0);


        transitionController = new Rectangle();
        transition = "enter";
        transitionController.x = 0;
        transitionController.y = 0;
        transitionAlpha = 1.0f;

        bmpFnt = new BitmapFont(Gdx.files.internal("pxlFnt.fnt"));
        layout = new GlyphLayout();

        this.backTexture = new Texture(Gdx.files.internal("BackBtn.png"));
        this.backPressedTexture = new Texture(Gdx.files.internal("BackPressedBtn.png"));

        this.titleTexture = new Texture(Gdx.files.internal("OptionsTitle.png"));

        this.backButton = new ImageButton(new TextureRegionDrawable(new TextureRegion(this.backTexture)), new TextureRegionDrawable(new TextureRegion(this.backPressedTexture)));
        this.backButton.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                if (transition == "idle"){
                    transition = "exit";
                }

            }
        });

        this.SFXTexture = new Texture(Gdx.files.internal("sfxBtn.png"));
        this.SFXPressedTexture = new Texture(Gdx.files.internal("sfxPressedBtn.png"));

        this.SFXButton = new ImageButton(new TextureRegionDrawable(new TextureRegion(this.SFXTexture)), new TextureRegionDrawable(new TextureRegion(this.SFXPressedTexture)));
        this.SFXButton.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                soundEna = !soundEna;
            }
        });

        this.SoundTexture = new Texture(Gdx.files.internal("sndBtn.png"));
        this.SoundPressedTexture = new Texture(Gdx.files.internal("sndPressedBtn.png"));

        this.SoundButton = new ImageButton(new TextureRegionDrawable(new TextureRegion(this.SoundTexture)), new TextureRegionDrawable(new TextureRegion(this.SoundPressedTexture)));
        this.SoundButton.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                musicEna = !musicEna;

            }
        });

        this.backButton.setPosition(80.0f-backButton.getWidth()/2,-80.0f-backButton.getHeight()/2);
        this.backButton.getImage().scaleBy(3.0f);
        this.stage.addActor(this.backButton);

        this.SFXButton.getImage().scaleBy(3.0f);
        this.SoundButton.getImage().scaleBy(3.0f);

        this.SFXButton.setPosition((stage.getWidth()/2)-(SFXButton.getWidth()*3/1.5f),310.0f);
        this.SoundButton.setPosition((stage.getWidth()/2)-(SFXButton.getWidth()*3/1.5f),180.0f);


        this.stage.addActor(this.backButton);
        this.stage.addActor(this.SFXButton);
        this.stage.addActor(this.SoundButton);


        SFXButton.addAction(Actions.sequence(Actions.fadeOut(0.0f),Actions.fadeIn(1.0f)));
        SoundButton.addAction(Actions.sequence(Actions.fadeOut(0.0f),Actions.delay(0.25f),Actions.fadeIn(1.0f)));
        backButton.addAction(Actions.sequence(Actions.delay(0.25f),Actions.moveBy(0.0f,120.0f,0.75f, Interpolation.sineOut)));

        if (!soundEna){
            SFXButton.setColor(1.0f,1.f,1.f,.5f);
        } else {
            SFXButton.setColor(1.0f,1.f,1.f,1.f);

        }

        if (!musicEna){
            SoundButton.setColor(1.0f,1.f,1.f,.5f);
        } else {
            SoundButton.setColor(1.0f,1.f,1.f,1.f);

        }
        txt = "Crafted by\nDiego Perez  - Ian Gonzalez\nCarlos Flores - Alex Vallejo";
        txt2 = "";
        i = 0.0;

    }

    public void resize(int width, int height) {
        super.resize(width, height);
        this.stage.getViewport().update(width, height, true);
    }

    public void render(float delta) {

        super.render(delta);
        this.clearScreen();


        if (transition == "exit") {
            transitionController.y -= Math.abs(0-transitionController.y/6);
            if (transitionAlpha <1.0f){
                transitionAlpha +=0.1f;

            }
            if (transitionController.y <= 10) {
                Preferences prefs = Gdx.app.getPreferences("MyPreferences");
                prefs.putBoolean("soundena",soundEna);
                prefs.putBoolean("musicena",musicEna);
                prefs.flush();

                OptionsScreen.this.game.setScreen(new MainMenuScreen(OptionsScreen.this.game));
                OptionsScreen.this.dispose();
            }
        }
        if (transition == "enter") {
            transitionController.y -= 72;

            if (transitionController.y <= -720) {
                if (transition == "enter")
                    transition = "idle";
                transitionController.y = 720;
            }
        }

        if ((transitionAlpha > 0.02f) && (transition == "enter" || transition == "idle")){
            transitionAlpha -=0.02f;
        }



        batch.begin();

        batch.draw(backgroundTexture,0.0f,0.0f,xx,yy);


        if (i<txt.length()){

            if (i == Math.floor(i)) {
                int x = (int) i;
                txt2 += txt.charAt(Integer.valueOf((x)));
            }
            i+=0.5;
        }
        bmpFnt.getData().setScale(xx/1280);
        bmpFnt.draw(batch,txt2,xx/2,yy/5,1.0f,1,false);
        batch.draw(titleTexture,(xx/2)-(titleTexture.getWidth()*(xx/320)/2),(yy*0.754f)-(titleTexture.getHeight()*(xx/320)/2),titleTexture.getWidth()*(xx/320),titleTexture.getHeight()*(xx/320));
        batch.end();

        if (!soundEna){
            SFXButton.setColor(1.0f,1.f,1.f,.5f);
        } else {
            SFXButton.setColor(1.0f,1.f,1.f,1.f);

        }

        if (!musicEna){
            SoundButton.setColor(1.0f,1.f,1.f,.5f);
        } else {
            SoundButton.setColor(1.0f,1.f,1.f,1.f);

        }

        this.stage.act(delta);
        this.stage.draw();

        batch.begin();
        batch.setColor(1.0f,1.0f,1.0f, transitionAlpha);
        batch.draw(transitionBackground,0.0f,0.0f,xx,yy);
        batch.setColor(1.0f,1.0f,1.0f,1.0f);
        batch.draw(transitionTexture,transitionController.x,transitionController.y,xx,yy);
        batch.end();
    }

    public void dispose() {
        super.dispose();
        this.stage.dispose();
        this.backgroundTexture.dispose();
        this.backTexture.dispose();
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(16384);
    }
}
