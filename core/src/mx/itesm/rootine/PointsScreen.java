package mx.itesm.rootine;

/**
 * Created by prez on 2/25/18.
 */

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class PointsScreen extends ScreenAdapter {
    private final Game game;
    private float xx = Gdx.graphics.getWidth();
    private float yy = Gdx.graphics.getHeight();
    private Stage stage;
    private Texture backgroundTexture;



    BitmapFont bmpFnt;
    BitmapFont bmpFnt2;
    GlyphLayout layout;



    private Texture transitionTexture;
    private Texture transitionBackground;
    private Texture background;
    private String transition;
    private Rectangle transitionController;
    private SpriteBatch batch;

    private Float transitionAlpha;

    private String txt;
    private double i;
    private float textAlpha;
    private  float txtAlpha;

    private int screenTime;
    private Sound letter;
    private int noMiniGames;
    private int noDays;
    private boolean gotoDays;
    private String lastGame;
    private int points;
    private int pointsToAdd;
    private int lives;
    private int prevLives;

    // Aqui defines cuantas filas y columnas tiene el SpriteSheet
    private static final int SHEET_COLUMNS = 8, SHEET_ROWS = 1;
    private static final int SHEET_COLUMNS2 = 15, SHEET_ROWS2 = 1;

    Animation<TextureRegion> spriteAnimation;
    Texture spriteSheet;

    Animation<TextureRegion> spriteAnimation2;
    Texture spriteSheet2;
    SpriteBatch spriteBatch;

    // Esta variable define el tiempo que requiere el codigo en realizar toda la animación
    float stateTime;
    private float gameSpeed;
    private Integer score;

    private Sound pointsound;
    private Sound loseSnd;
    private boolean punchsound = false;
    private boolean checked = false;
    private Preferences prefs;


    public PointsScreen(Game game) {
        this.game = game;
    }

    public void show() {
        super.show();

        spriteSheet = new Texture(Gdx.files.internal("playerHeadBob_strip8.png"));
        spriteSheet2 = new Texture(Gdx.files.internal("playerHeadDead_strip8.png"));


        //Divide la spritesheet en una matriz con las diferentes texturas
        TextureRegion[][] temp = TextureRegion.split(spriteSheet, spriteSheet.getWidth()/SHEET_COLUMNS,
                spriteSheet.getHeight()/SHEET_ROWS);

        //Acomoda los sprites en un array para poder iterarlos
        TextureRegion[] spriteFrames = new TextureRegion[SHEET_COLUMNS*SHEET_ROWS];
        int index = 0;
        for (int i = 0; i < SHEET_ROWS; i++){
            for (int j = 0; j < SHEET_COLUMNS; j++){
                spriteFrames[index++] = temp[i][j];
            }
        }

        // ide la spritesheet en una matriz con las diferentes texturas
        TextureRegion[][] temp2 = TextureRegion.split(spriteSheet2, spriteSheet2.getWidth()/SHEET_COLUMNS2,
                spriteSheet2.getHeight()/SHEET_ROWS2);

        //Acomoda los sprites en un array para poder iterarlos
        TextureRegion[] spriteFrames2 = new TextureRegion[SHEET_COLUMNS2*SHEET_ROWS2];
        int index2 = 0;
        for (int i = 0; i < SHEET_ROWS2; i++){
            for (int j = 0; j < SHEET_COLUMNS2; j++){
                spriteFrames2[index2++] = temp2[i][j];
            }
        }
        spriteAnimation = new Animation<TextureRegion>(0.075f, spriteFrames);

        spriteAnimation2 = new Animation<TextureRegion>(0.075f, spriteFrames2);

        spriteBatch = new SpriteBatch();
        stateTime = 0f;

        this.pointsound = Gdx.audio.newSound(Gdx.files.internal("points.wav"));
        pointsound.play(0.f);

        this.loseSnd = Gdx.audio.newSound(Gdx.files.internal("spunch.wav"));
        loseSnd.play(0.f);

        gotoDays = false;

        prefs = Gdx.app.getPreferences("MyPreferences");
        noMiniGames = prefs.getInteger("minigamesNo",0);
        lastGame  = prefs.getString("LastGame","win");
        points = prefs.getInteger("score",0);
        lives = prefs.getInteger("livesNo",3);
        prevLives = prefs.getInteger("prevlivesNo",3);
        gameSpeed = prefs.getFloat("gameSpeed",2);
        score = prefs.getInteger("hiscore",0);

        pointsToAdd = prefs.getInteger("addPoints",0);

        Gdx.app.log("LIVES",String.valueOf(lives));
        Gdx.app.log("PREVLIVES",String.valueOf(prevLives));
        Gdx.app.log("pointstoadd",String.valueOf(pointsToAdd));

        if (pointsToAdd == 0){
            lives -=1;
            Gdx.app.log("","huh");
        }
        Gdx.app.log(String.valueOf(prevLives),String.valueOf(lives));

        if (noMiniGames > 8){
            noDays = prefs.getInteger("dayNo",1);
            noDays+=1;

            prefs.putInteger("dayNo",noDays);
            prefs.putInteger("minigamesNo",0);
            prefs.putFloat("gameSpeed",gameSpeed + gameSpeed/15);

            gotoDays = true;
            noMiniGames = 0;

        }
        prefs.flush();


        this.stage = new Stage(new FitViewport(1280.0F, 720.0F));
        Gdx.input.setInputProcessor(this.stage);
        this.backgroundTexture = new Texture(Gdx.files.internal("OptionsBg.png"));
        Image background = new Image(this.backgroundTexture);

        this.background = new Texture(Gdx.files.internal("menuPointsBG.png"));

        batch = new SpriteBatch();
        this.transitionTexture = new Texture(Gdx.files.internal("TransitionTitle.png"));
        this.transitionBackground = new Texture(Gdx.files.internal("TransitionScreen2.png"));


        transitionController = new Rectangle();
        transition = "enter";
        transitionController.x = 0;
        transitionController.y = 0;
        transitionAlpha = 1.0f;

        bmpFnt = new BitmapFont(Gdx.files.internal("pxlFnt.fnt"));
        bmpFnt2 = new BitmapFont(Gdx.files.internal("pxelFnt2.fnt"));

        layout = new GlyphLayout();


        txt = "POINTS";
        i = 0.0;
        textAlpha = 0.0f;
        txtAlpha = 0.0f;

        if (lives == 0) {
            screenTime = 200;
        }
        else {
            screenTime = 200 - (points/200)*10;
        }

    }

    public void resize(int width, int height) {
        super.resize(width, height);
        this.stage.getViewport().update(width, height, true);
    }

    public void render(float delta) {

        super.render(delta);
        this.clearScreen();



        if (transition == "exit") {
            transitionController.y -= Math.abs(0-transitionController.y/6);
            if (transitionAlpha <1.0f){
                transitionAlpha += 0.1f;

            }
            if (transitionController.y <= 10) {
                prefs.putInteger("score", points);
                prefs.putInteger("prevlivesNo",lives);
                prefs.putInteger("livesNo",lives);
                prefs.flush();
                if (lives > 0) {
                    if (gotoDays == false) {

                        if (noMiniGames == 1) {
                            PointsScreen.this.game.setScreen(new coffeeMiniGame(PointsScreen.this.game));
                            PointsScreen.this.dispose();
                        }

                        if (noMiniGames == 2) {
                            PointsScreen.this.game.setScreen(new brushTeethMiniGame(PointsScreen.this.game));
                            PointsScreen.this.dispose();
                        }

                        if (noMiniGames == 3) {
                            PointsScreen.this.game.setScreen(new BusMiniGame(PointsScreen.this.game));
                            PointsScreen.this.dispose();
                        }
                        if (noMiniGames == 4) {
                            PointsScreen.this.game.setScreen(new Office1MiniGame(PointsScreen.this.game));
                            PointsScreen.this.dispose();
                        }
                        if (noMiniGames == 5) {
                            PointsScreen.this.game.setScreen(new OfficeSortMiniGame(PointsScreen.this.game));
                            PointsScreen.this.dispose();
                        }
                        if (noMiniGames == 6) {
                            PointsScreen.this.game.setScreen(new streetMiniGame(PointsScreen.this.game));
                            PointsScreen.this.dispose();
                        }
                        if (noMiniGames == 7) {
                            PointsScreen.this.game.setScreen(new DrinkMiniGame(PointsScreen.this.game));
                            PointsScreen.this.dispose();
                        }
                        if (noMiniGames == 8) {
                            PointsScreen.this.game.setScreen(new FindDarkMiniGame(PointsScreen.this.game));
                            PointsScreen.this.dispose();
                        }
                    }

                    if (gotoDays == true) {
                        PointsScreen.this.game.setScreen(new DayScreen((PointsScreen.this.game)));
                        PointsScreen.this.dispose();
                    }


                } else {
                    prefs.putInteger("prevlivesNo",3);
                    prefs.putInteger("livesNo",3);
                    prefs.flush();
                    PointsScreen.this.game.setScreen(new MainMenuScreen(PointsScreen.this.game));
                    PointsScreen.this.dispose();

                }

            }
        }
        if (transition == "enter") {
            transitionController.y -= 72;

            if (transitionController.y <= -720) {
                if (transition == "enter")
                    transition = "idle";
                transitionController.y = 720;
            }
        }

        if ((transitionAlpha > 0.02) && (transition == "enter" || transition == "idle")){
            transitionAlpha -=0.02f;
        }
        screenTime -=1;
        if (screenTime <=0){
            transition = "exit";
        }

        this.stage.act(delta);
        this.stage.draw();
        batch.begin();
        batch.draw(background,0,0,xx,yy);


        if (i == txt.length()){
                textAlpha += 0.02;
                txtAlpha = (float) Math.sin(textAlpha);
        }
        if (pointsToAdd > 0 ){

            points +=5;
            pointsToAdd-=5;
            if ((pointsToAdd/33)==0)
            pointsound.play(0.5f, (100 - pointsToAdd) / 100, 0.f);


        } else {
            Preferences prefs = Gdx.app.getPreferences("MyPreferences");
            prefs.putInteger("score", points);
            prefs.putInteger("livesNo", lives);
            if (points > score){
                prefs.putInteger("hiscore",points);
            }
            prefs.flush();
        }

        bmpFnt.getData().setScale(xx/1280);
        bmpFnt.draw(batch,txt,xx/2,yy/2 -yy*.137f,1.0f,1,false);
        bmpFnt2.getData().setScale(xx/1280);
        bmpFnt2.draw(batch,String.valueOf(points),xx/2,yy/2 -yy*.197f,1.0f,1,false);

        bmpFnt.setColor(0.9f,0.02f,0.35f,txtAlpha);
        bmpFnt.draw(batch,txt,xx/2,yy/2,1.0f,1,false);
        bmpFnt.setColor(Color.WHITE);

        if (lives == 0  && screenTime < 100) {
            bmpFnt2.getData().setScale(xx / 640);
            bmpFnt2.draw(batch, "GAME OVER", xx / 2 + MathUtils.random(-5,5), yy * 0.75f+ MathUtils.random(-5,5), 1.0f, 1, false);
        }


        stateTime += Gdx.graphics.getDeltaTime();
        TextureRegion currentFrame = spriteAnimation.getKeyFrame(stateTime, true);
        TextureRegion currentFrame2 = spriteAnimation2.getKeyFrame(stateTime, false);

        //DRAW LIVES
        if (lives >= 1) {
            batch.draw(currentFrame, xx / 2 - currentFrame.getRegionWidth() * (Math.round(xx / 427)) / 2, yy / 2, currentFrame.getRegionWidth() * (Math.round(xx / 427)), currentFrame.getRegionHeight() * (Math.round(xx / 427)));
        } else if (prevLives >=1) {
            batch.draw(currentFrame2, xx / 2 - currentFrame2.getRegionWidth() * (Math.round(xx / 427)) / 2, yy / 2, currentFrame2.getRegionWidth() * (Math.round(xx / 427)), currentFrame2.getRegionHeight() * (Math.round(xx / 427)));
            if (!punchsound){
                loseSnd.play(1.f);
                punchsound = true;
            }
        }

        if (lives >= 2) {
            batch.draw(currentFrame, xx * 4 / 10 - currentFrame.getRegionWidth() * (Math.round(xx / 427)) / 2, yy / 2, currentFrame.getRegionWidth() * (Math.round(xx / 427)), currentFrame.getRegionHeight() * (Math.round(xx / 427)));
        } else if (prevLives >= 2){
            batch.draw(currentFrame2, xx * 4 / 10 - currentFrame2.getRegionWidth() * (Math.round(xx / 427)) / 2, yy / 2, currentFrame2.getRegionWidth() * (Math.round(xx / 427)), currentFrame2.getRegionHeight() * (Math.round(xx / 427)));
            if (!punchsound){
                loseSnd.play(1.f);
                punchsound = true;
            }
        }

        if (lives == 3) {
            batch.draw(currentFrame, xx * 6 / 10 - currentFrame.getRegionWidth() * (Math.round(xx / 427)) / 2, yy / 2, currentFrame.getRegionWidth() * (Math.round(xx / 427)), currentFrame.getRegionHeight() * (Math.round(xx / 427)));
        } else if (prevLives == 3){
            batch.draw(currentFrame2, xx * 6 / 10 - currentFrame2.getRegionWidth() * (Math.round(xx / 427)) / 2, yy / 2, currentFrame2.getRegionWidth() * (Math.round(xx / 427)), currentFrame2.getRegionHeight() * (Math.round(xx / 427)));
            if (!punchsound){
                loseSnd.play(1.f);
                punchsound = true;
            }
        }
        batch.setColor(1.0f,1.0f,1.0f, transitionAlpha);
        batch.draw(transitionBackground,0.0f,0.0f,xx,yy);
        batch.setColor(1.0f,1.0f,1.0f,1.0f);
        batch.draw(transitionTexture,transitionController.x,transitionController.y,xx,yy);
        batch.end();
    }

    public void dispose() {
        super.dispose();
        this.stage.dispose();
        this.backgroundTexture.dispose();
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(16384);
    }
}
