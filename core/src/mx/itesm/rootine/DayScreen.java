package mx.itesm.rootine;

/**
 * Created by prez on 2/25/18.
 */

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class DayScreen extends ScreenAdapter {
    private final Game game;

    private float xx = Gdx.graphics.getWidth();
    private float yy = Gdx.graphics.getHeight();
    private Stage stage;

    private Texture backgroundTexture;



    BitmapFont bmpFnt;
    GlyphLayout layout;



    private Texture transitionTexture;
    private Texture transitionBackground;
    private String transition;
    private Rectangle transitionController;
    private SpriteBatch batch;

    private Float transitionAlpha;

    private String txt;
    private String txt2;
    private double i;
    private float textAlpha;
    private  float txtAlpha;

    private int screenTime;
    private Sound letter;
    private int shake;

    public DayScreen(Game game) {
        this.game = game;
    }

    public void show() {
        super.show();
        this.stage = new Stage(new FitViewport(1280.0F, 720.0F));
        Gdx.input.setInputProcessor(this.stage);
        this.backgroundTexture = new Texture(Gdx.files.internal("OptionsBg.png"));
        Image background = new Image(this.backgroundTexture);

        batch = new SpriteBatch();
        this.transitionTexture = new Texture(Gdx.files.internal("TransitionTitle.png"));
        this.transitionBackground = new Texture(Gdx.files.internal("TransitionScreen2.png"));
        this.letter = Gdx.audio.newSound(Gdx.files.internal("letter.wav"));
        letter.play(0.f);

        transitionController = new Rectangle();
        transition = "enter";
        transitionController.x = 0;
        transitionController.y = 0;
        transitionAlpha = 1.0f;

        bmpFnt = new BitmapFont(Gdx.files.internal("pxlFnt.fnt"));
        layout = new GlyphLayout();

        Preferences prefs = Gdx.app.getPreferences("MyPreferences");

        switch(prefs.getInteger("dayNo",1)){
            case 1: txt = "DAY ONE";
                    shake = 0;
                    break;
            case 2: txt = "DAY TWO";
                    shake = 0;
                    break;
            case 3: txt = "DAY THREE";
                    shake = 2;
                    break;
            case 4: txt = "DAY FOUR";
                    shake = 4;
                    break;
            case 5: txt = "DAY FIVE";
                    shake = 6;
                    break;

        }

        txt2 = "";
        i = 0.0;
        textAlpha = 0.0f;
        txtAlpha = 0.0f;
//DEVELOPER MODE PARA QUE ENTER MAS RAPIDO
        screenTime = 150;

    }

    public void resize(int width, int height) {
        super.resize(width, height);
        this.stage.getViewport().update(width, height, true);
    }

    public void render(float delta) {

        super.render(delta);
        this.clearScreen();


        if (transition == "exit") {
            transitionController.y -= Math.abs(0-transitionController.y/6);
            if (transitionAlpha <1.0f){
                transitionAlpha +=0.1f;

            }
            if (transitionController.y <= 10) {
                DayScreen.this.game.setScreen(new MiniGame1(DayScreen.this.game));
                DayScreen.this.dispose();
            }
        }
        if (transition == "enter") {
            transitionController.y -= 72;

            if (transitionController.y <= -720) {
                if (transition == "enter")
                    transition = "idle";
                transitionController.y = 720;
            }
        }

        if ((transitionAlpha > 0.02) && (transition == "enter" || transition == "idle")){
            transitionAlpha -=0.02f;
        }
        screenTime -=1;
        if (screenTime <=0){
            transition = "exit";
        }

        this.stage.act(delta);
        this.stage.draw();
        batch.begin();

        if (i<txt.length()){

            if (i == Math.floor(i)) {
                int x = (int) i;
                txt2 += txt.charAt(Integer.valueOf((x)));

                letter.play();


            }
            i+=0.25;
      }

        if (i == txt.length()){
                textAlpha += 0.02;
                txtAlpha = (float) Math.sin(textAlpha);
        }

        bmpFnt.getData().setScale(xx/1280);
        ///DEBUG TEXT SHAKE
        bmpFnt.draw(batch,txt2,xx/2+ MathUtils.random(-shake,shake),yy/2+ MathUtils.random(-shake,shake),1.0f,1,false);

        bmpFnt.setColor(0.9f,0.02f,0.35f,txtAlpha);
        bmpFnt.draw(batch,txt2,xx/2+ MathUtils.random(-shake,shake),yy/2+ MathUtils.random(-shake,shake),1.0f,1,false);
        bmpFnt.setColor(Color.WHITE);

        batch.setColor(1.0f,1.0f,1.0f, transitionAlpha);
        batch.draw(transitionBackground,0.0f,0.0f,xx,yy);
        batch.setColor(1.0f,1.0f,1.0f,1.0f);
        batch.draw(transitionTexture,transitionController.x,transitionController.y,xx,yy);
        batch.end();
    }

    public void dispose() {
        super.dispose();
        this.stage.dispose();
        this.backgroundTexture.dispose();
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(16384);
    }
}
