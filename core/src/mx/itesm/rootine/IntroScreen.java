package mx.itesm.rootine;

/**
 * Created by prez on 2/25/18.
 */

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class IntroScreen extends ScreenAdapter {

    private float xx = Gdx.graphics.getWidth();
    private float yy = Gdx.graphics.getHeight();
    private Stage stage;
    private Texture backgroundTexture;
    private Texture logoTexture;

    private Texture transitionTexture;
    private Texture transitionBackground;
    private String transition;
    private Rectangle transitionController;
    private Float transitionAlpha;
    private final Game game;
    private Sound bgmsc;
    public IntroScreen(Game game) {
        this.game = game;
    }



    private SpriteBatch batch;


    public void show() {

        super.show();
        //Reset Variables
        //Reset
        Preferences prefs = Gdx.app.getPreferences("MyPreferences");
        prefs.putInteger("roomvisits",0);
        prefs.putBoolean("bgmStarted",false);
        prefs.flush();

        this.bgmsc = Gdx.audio.newSound(Gdx.files.internal("sMusic.wav"));
        bgmsc.play(0.f);

        this.stage = new Stage(new FitViewport(1280.0F, 720.0F));
        Gdx.input.setInputProcessor(this.stage);

        this.backgroundTexture = new Texture(Gdx.files.internal("tec.png"));
        this.logoTexture = new Texture(Gdx.files.internal("MeteoroidArts.png"));


        Image background = new Image(this.backgroundTexture);

        Image logo = new Image(this.logoTexture);
        logo.scaleBy(3.0f);
        this.stage.addActor(background);
        this.stage.addActor(logo);
        logo.getColor().a = 0.0F;

        batch = new SpriteBatch();

        this.transitionTexture = new Texture(Gdx.files.internal("TransitionTitle.png"));
        this.transitionBackground = new Texture(Gdx.files.internal("TransitionScreen2.png"));
        transitionController = new Rectangle();
        //DEVELOPER MODE PARA QUE ENTER MAS RAPIDO enter <-> exit
        transition = "idle";

        transitionController.x = 0;
        transitionController.y = 720;
        transitionAlpha = 0.0f;

        background.addAction(Actions.sequence(Actions.delay(0.5F), Actions.fadeOut(1.0f)));

        logo.addAction(Actions.sequence(Actions.delay(1.2F), Actions.fadeIn(0.5F), Actions.delay(1.5F),
                Actions.run(new Runnable() {
            public void run() {
                if (transition == "idle") {
                    transition = "exit";
                }
            }
        })));

    }

    public void resize(int width, int height) {
        super.resize(width, height);
        this.stage.getViewport().update(width, height, true);
    }

    public void render(float delta) {
        super.render(delta);
        this.clearScreen();
        this.stage.act(delta);
        this.stage.draw();

        if (transition == "exit") {
            transitionController.y -= Math.abs(0-transitionController.y/6);
            if (transitionAlpha <1.0f){
                transitionAlpha +=0.1f;
            }

            if (transitionController.y <= 10) {
                IntroScreen.this.game.setScreen(new MainMenuScreen(IntroScreen.this.game));
                IntroScreen.this.dispose();
            }
        }
        if (transition == "enter") {
            transitionController.y -= 72;

            if (transitionController.y <= -720) {
                if (transition == "enter")
                    transition = "idle";
                transitionController.y = 720;
            }
        }
        batch.begin();
        batch.setColor(1.0f,1.0f,1.0f, transitionAlpha);
        batch.draw(transitionBackground,0.0f,0.0f,xx,yy);
        batch.setColor(1.0f,1.0f,1.0f,1.0f);
        batch.draw(transitionTexture,transitionController.x,transitionController.y,xx,yy);
        batch.end();
    }

    public void dispose() {
        super.dispose();
        this.stage.dispose();
        this.backgroundTexture.dispose();
        this.logoTexture.dispose();
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(16384);
    }
}
