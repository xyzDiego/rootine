package mx.itesm.rootine;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;

/**
 * Created by Ian on 04/04/2018.
 */

public class brushTeethMiniGame extends ScreenAdapter{

    private final Game game;
    private float xx = Gdx.graphics.getWidth();
    private float yy = Gdx.graphics.getHeight();
    private Stage stage;
    private Texture backgroundTexture;
    private Texture bg;
    private Texture time;

    private Texture brazoImg, characterImg, yellowImg;

    private Rectangle brazo;


    BitmapFont bmpFnt;
    GlyphLayout layout;

    private double maxSpd;
    private double movSpd;

    private Texture transitionTexture;
    private Texture transitionBackground;
    private String transition;
    private Rectangle transitionController;
    private SpriteBatch batch;

    private Float transitionAlpha;

    private int screenTime;
    private double seconds;
    private int minutes;
    private int noMiniGames;
    private int noDays;
    private String gameState;
    private int dir = 0;
    private float progress = 1;
    private int instructionPos = -50;
    private Float instructionAlpha;
    private int points;

    private Texture Failed;
    private Texture Win;
    private Texture FailedSym;
    private Texture WinSym;
    private Texture pauseBtnTexture;
    private Texture pauseBtnPressedTexture;
    private ImageButton pauseBtn;
    private Texture pausedTitle;
    private int alarm = 120;



    private Texture resumeBtnTexture;
    private Texture resumeBtnPressedTexture;
    private ImageButton resumeBtn;
    private Texture blk;


    private Texture menuBtnTexture;
    private Texture menuBtnPressedTexture;
    private ImageButton menuBtn;
    private int startTrans = 50;
    private float gameSpeed;
    private Boolean paused = false;
    private float pausedAlpha;
    private Rectangle failWinController;

    private Sound win;
    private Sound loss;
    private boolean fanfarePlay = false;




    public brushTeethMiniGame(Game game) {
        this.game = game;
    }

    public void show() {

        super.show();

        characterImg = new Texture(Gdx.files.internal("personajeDientes.png"));
        brazoImg = new Texture(Gdx.files.internal("manoDientes.png"));
        yellowImg = new Texture(Gdx.files.internal("suciedadDientes.png"));

        Preferences prefs = Gdx.app.getPreferences("MyPreferences");
        noMiniGames = prefs.getInteger("minigamesNo", 0);
        noMiniGames += 1;
        prefs.putInteger("minigamesNo", noMiniGames);
        gameSpeed = prefs.getFloat("gameSpeed",2);

        prefs.flush();

        this.stage = new Stage(new FitViewport(1280.0f,720.0f));
        Gdx.input.setInputProcessor(this.stage);
        this.backgroundTexture = new Texture(Gdx.files.internal("OptionsBg.png"));
        Image background = new Image(this.backgroundTexture);

        batch = new SpriteBatch();
        this.transitionTexture = new Texture(Gdx.files.internal("TransitionTitle.png"));
        this.transitionBackground = new Texture(Gdx.files.internal("TransitionScreen2.png"));
        this.time = new Texture(Gdx.files.internal("timeBar.png"));
        this.bg = new Texture(Gdx.files.internal("m_dientesBG.png"));

        this.Failed = new Texture(Gdx.files.internal("failedTxt.png"));
        this.Win = new Texture(Gdx.files.internal("AwesomeTxt.png"));
        this.FailedSym = new Texture(Gdx.files.internal("FailedSymbol.png"));
        this.WinSym = new Texture(Gdx.files.internal("AwesomeSymbol.png"));
        this.blk = new Texture(Gdx.files.internal("blk.png"));

        this.win = Gdx.audio.newSound(Gdx.files.internal("win.wav"));
        win.play(0.f);

        this.loss = Gdx.audio.newSound(Gdx.files.internal("lifelose.wav"));
        loss.play(0.f);


        transitionController = new Rectangle();
        transition = "enter";
        transitionController.x = 0;
        transitionController.y = 0;
        transitionAlpha = 1.0f;
        instructionAlpha = 1.0f;

        gameState = "running";
        failWinController = new Rectangle();
        failWinController.setSize(1,1);
        failWinController.x = 0;
        failWinController.y = -600;



        bmpFnt = new BitmapFont(Gdx.files.internal("pxelFnt2.fnt"));
        layout = new GlyphLayout();


        points = prefs.getInteger("score",0);
        screenTime = 250;

        batch = new SpriteBatch();


        this.brazo = new Rectangle();
        brazo.x = 0;
        brazo.y = 0;

        movSpd = 0;
        maxSpd = 100;

        //Pause Button

        this.pauseBtnTexture = new Texture(Gdx.files.internal("pauseBtn.png"));
        this.pauseBtnPressedTexture = new Texture(Gdx.files.internal("pauseBtnPressed.png"));
        this.pausedTitle = new Texture(Gdx.files.internal("PausedTitle.png"));


        this.pauseBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(this.pauseBtnTexture)), new TextureRegionDrawable(new TextureRegion(this.pauseBtnPressedTexture)));
        this.pauseBtn.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                if (transition == "idle" && screenTime > 0)
                    paused = !paused;

            }
        });

        this.pauseBtn.setPosition(stage.getWidth()-80.0f-pauseBtn.getWidth()/2,-80.0f-pauseBtn.getHeight()/2);
        this.pauseBtn.getImage().scaleBy(2.0f);
        this.stage.addActor(this.pauseBtn);
        this.pauseBtn.addAction(Actions.sequence(Actions.delay(0.25f),Actions.moveBy(0.0f,120.0f,0.75f, Interpolation.sineOut)));

        pausedAlpha = 0.0f;

        //Resume Button

        this.resumeBtnTexture = new Texture(Gdx.files.internal("resumeBtn.png"));
        this.resumeBtnPressedTexture = new Texture(Gdx.files.internal("resumePressedBtn.png"));


        this.resumeBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(this.resumeBtnTexture)), new TextureRegionDrawable(new TextureRegion(this.resumeBtnPressedTexture)));
        this.resumeBtn.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                if (transition == "idle" && screenTime > 0 && paused)
                    paused = false;

            }
        });

        this.resumeBtn.getImage().scaleBy(3.0f);
        this.resumeBtn.setPosition(stage.getWidth()/2 -(resumeBtn.getWidth()*3/1.5f),stage.getHeight()/2-resumeBtn.getHeight()/2);
        this.stage.addActor(this.resumeBtn);
        resumeBtn.setDisabled(true);
        resumeBtn.setColor(1.0f,1.0f,1.0f,0.0f);

        //Menu Button

        this.menuBtnTexture = new Texture(Gdx.files.internal("menuBtn.png"));
        this.menuBtnPressedTexture = new Texture(Gdx.files.internal("menuPressedBtn.png"));


        this.menuBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(this.menuBtnTexture)), new TextureRegionDrawable(new TextureRegion(this.menuBtnPressedTexture)));
        this.menuBtn.addListener(new ActorGestureListener() {
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                if (transition == "idle" && screenTime > 0) {
                    brushTeethMiniGame.this.game.setScreen(new MainMenuScreen(brushTeethMiniGame.this.game));
                    brushTeethMiniGame.this.dispose();
                }

            }
        });

        this.menuBtn.getImage().scaleBy(3.0f);
        this.menuBtn.setPosition(stage.getWidth()/2 -(menuBtn.getWidth()*3/1.5f),stage.getHeight()/4-menuBtn.getHeight()/2);
        this.stage.addActor(this.menuBtn);
        menuBtn.setDisabled(true);
        menuBtn.setColor(1.0f,1.0f,1.0f,0.0f);



    }

    public void resize(int width, int height) {
        super.resize(width, height);
        this.stage.getViewport().update(width, height, true);
    }

    public void render(float delta) {

        super.render(delta);
        this.clearScreen();


        if (transition == "exit") {
            transitionController.y -= Math.abs(0 - transitionController.y / 6);
            if (transitionAlpha < 1.0f) {
                transitionAlpha += 0.1f;

            }
            if (transitionController.y <= 10) {
                Preferences prefs = Gdx.app.getPreferences("MyPreferences");
                if (gameState == "win") {
                    prefs.putInteger("addPoints", 100);
                }if (gameState == "failed") {
                    prefs.putInteger("addPoints", 0);
                }
                prefs.flush();
                brushTeethMiniGame.this.game.setScreen(new PointsScreen(brushTeethMiniGame.this.game));
                brushTeethMiniGame.this.dispose();
            }
        }
        if (transition == "enter") {
            transitionController.y -= 72;

            if (transitionController.y <= -720) {
                if (transition == "enter")
                    transition = "idle";
                transitionController.y = 720;
            }
        }

        if ((transitionAlpha > 0.02) && (transition == "enter" || transition == "idle")) {
            transitionAlpha -= 0.02f;
        }

        if (!paused) {

            screenTime -= gameSpeed*0.75;

            if (screenTime <= 0) {
                if (gameState != "win") {
                    gameState = "failed";
                }
            }
        }

        if (instructionPos < 80 && transition == "idle")   //MOVE THE MINIGAME INSTRUCTION INTO SCREEN
            instructionPos += Math.abs((80 - instructionPos) / 16);
        if (screenTime < 150 && instructionAlpha > 0.02){
            instructionAlpha -=0.02f;
        }

        if (screenTime <= 0 && startTrans <= 0) //MOVE THE FAIL/WIN TEXT INTO SCREEN
            failWinController.y += Math.abs(failWinController.y / 12);


        if (progress < 0) {
            gameState = "win";
            progress = 0;
            screenTime = 0;
        }

        if (paused){

            stage.addActor(resumeBtn);
            resumeBtn.setDisabled(false);
            resumeBtn.setColor(1.0f,1.0f,1.0f,1.0f);
            stage.addActor(menuBtn);
            menuBtn.setDisabled(false);
            menuBtn.setColor(1.0f,1.0f,1.0f,1.0f);


            if (pausedAlpha < 0.8f){
                pausedAlpha += 0.05f;
            }

        } else {
            resumeBtn.setDisabled(true);
            resumeBtn.remove();

            resumeBtn.setColor(1.0f,1.0f,1.0f,0.0f);

            menuBtn.setDisabled(true);
            menuBtn.remove();
            menuBtn.setColor(1.0f,1.0f,1.0f,0.0f);


            if (pausedAlpha > 0.1f){
                pausedAlpha -= 0.1f;
            }
        }

        if (alarm > 0 && screenTime <= 0) {
            alarm -= 1;
        } else if (alarm == 0 && screenTime <= 0) {
            if (transition != "exit")
                transition = "exit";
        }
        if (screenTime <= 0){
            paused = false;
            this.pauseBtn.setDisabled(true);
            this.pauseBtn.addAction(Actions.sequence(Actions.moveBy(0.0f, -120.0f, 0.75f, Interpolation.sineOut)));
        }

        if (screenTime <= 0 && fanfarePlay == false){
            if (gameState == "win"){
                win.play(1.0f);

            }
            if (gameState == "failed"){
                loss.play(1.0f);
            }
            fanfarePlay = true;

        }

        batch.begin();
        batch.draw(bg,0.0f,0.0f,xx,yy);

        batch.draw(characterImg, 0, 0,xx,yy);

        batch.setColor(1.0f,1.0f,1.0f, progress);
        batch.draw(yellowImg, 0, 0,xx,yy);
        batch.setColor(1.0f,1.0f,1.0f,1.0f);

        batch.draw(brazoImg, brazo.x, -25,xx,yy);
        batch.draw(time,0,yy-yy/36,screenTime*(xx/250),yy/36);
        bmpFnt.setColor(1.0f,1.0f,1.0f,instructionAlpha);
        bmpFnt.getData().setScale(2.0f);
        bmpFnt.draw(batch,"BRUSH",instructionPos,yy*9/10);
        bmpFnt.getData().setScale(1.0f);
        bmpFnt.draw(batch,"YOUR TEETH",instructionPos,yy*9/10 - 70);
        bmpFnt.setColor(1.0f,1.0f,1.0f,1.0f);


        batch.setColor(1.0f,1.0f,1.0f, pausedAlpha);
        batch.draw(blk,0,0,xx,yy);
        batch.setColor(1.0f,1.0f,1.0f,1.0f);
        batch.draw(pausedTitle,xx/2-pausedTitle.getWidth()*(Math.round(xx / 320)) / 2, (yy*1.05f)-(pausedAlpha*yy*0.4f),pausedTitle.getWidth()*(Math.round(xx / 320)),pausedTitle.getHeight()*(Math.round(xx / 320)));
        batch.end();

        if (startTrans > 0) {
            startTrans -= 1;
        } else if (startTrans <= 0) {
            batch.begin();
            if (gameState == "failed") {
                batch.draw(FailedSym, 0.0f,Math.abs(failWinController.y), xx, yy);
                batch.draw(Failed, 0.0f, failWinController.y, xx, yy);
            }
            if (gameState == "win") {
                batch.draw(WinSym, 0.0f, Math.abs(failWinController.y), xx, yy);
                batch.draw(Win, 0.0f, failWinController.y, xx, yy);
            }
            batch.end();
        }

        batch.begin();
        batch.setColor(1.0f,1.0f,1.0f, transitionAlpha);
        batch.draw(transitionBackground,0.0f,0.0f,xx,yy);
        batch.setColor(1.0f,1.0f,1.0f,1.0f);
        batch.draw(transitionTexture,transitionController.x,transitionController.y,xx,yy);

        batch.end();


        if (!paused) {
            if (brazo.x == xx/25.6) {
                if (progress > 0)
                    progress -= 0.2*gameSpeed;
            }

            if (gameState == "running") {
                if (Gdx.input.isTouched() && yy -Gdx.input.getY() > yy/10) {

                    if (Gdx.input.getX() < xx / 2 && brazo.x > -xx/10) {
                        brazo.x -= xx/51.2;
                    }

                    if (Gdx.input.getX() >= xx / 2 && brazo.x < xx/5) {
                        brazo.x += xx/51.2;

                    }
                }

            }
        }



        this.stage.act(delta);
        this.stage.draw();

    }

    public void dispose() {
        super.dispose();
        this.stage.dispose();
        this.backgroundTexture.dispose();
    }



    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(16384);
    }
}

