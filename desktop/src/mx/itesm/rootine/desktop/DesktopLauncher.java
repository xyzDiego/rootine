package mx.itesm.rootine.desktop;


import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import mx.itesm.rootine.MenuDemo;

public class DesktopLauncher {
	public DesktopLauncher() {
	}

	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = 1280;
		config.height = 720;

		new LwjglApplication(new MenuDemo(), config);
	}
}
//